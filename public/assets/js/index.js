$(function() {
        VWCaddyBuild.Init();
});

var VWCaddyBuild = {
        
        "Page": "Home",
        "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
        "Alphabet": ["A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "I", "i", "J", "j", "K", "k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z"],
        "Numbers": ["0","1","2","3","4","5","6","7","8","9"],
        "CharacterMap": ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9","~","`","!","@","#","$","%","^","&","*","(",")","-","_","=","+",">",".","<",",","/","?","[","]","{","}","|"],
        "HashMapping": [],
        "ThumbsToLoad": 20,
        "ImagesLoaded": 0,
        "ImageLoadComplete": false,
        "VideosLoaded": 0,
        "VideoLoadComplete": false,
        "AccordionsSet": false,
                
        "Init": function() {
                var _this = this;
                if(this.Page == "Pictures") this.LoadThumbs({"type": "images"});
                if(this.Page == "Videos") this.LoadThumbs({"type": "videos"});
                this.Events();

                $(window).resize(function() { _this.WindowResize(); });
                $(window).scroll(function() { _this.WindowScroll(); });
        },
        
        "WindowResize": function() {
                if(this.Page == "Pictures") this.LoadThumbs({"type": "images"});
                if(this.Page == "Videos") this.LoadThumbs({"type": "videos"});
        },
        
        "WindowScroll": function() {
                if(this.Page == "Pictures") this.LoadThumbs({"type": "images"});
                if(this.Page == "Videos") this.LoadThumbs({"type": "videos"});
        },
        
        "LoadThumbs": function(props) {
        
                var data = (props.type == "images") ? buildImages : buildVideos;
                var container = (props.type == "images") ? "#build-thumbs" : "#build-videos";
                var loadedNum = (props.type == "images") ? this.ImagesLoaded : this.VideosLoaded;
                var loadComplete = (props.type == "images") ? this.ImageLoadComplete : this.VideoLoadComplete;
                var nextSet = 0;
                
                if(($(window).scrollTop() + $(window).height()) >= $(container).offset().top) {
                        if(!$(container).closest(".content-panel").attr("aria-hidden") || $(container).closest(".content-panel").attr("aria-hidden") == "false") {
                                if(!$(".thumb.loading").length) {
                                        if(!loadComplete) {
                                                nextSet = loadedNum + this.ThumbsToLoad;
                                                if(nextSet > data.length) {
                                                        nextSet = data.length;

                                                        if(props.type == "images") {
                                                                this.ImageLoadComplete = true;
                                                        } else {
                                                                this.VideoLoadComplete = true;
                                                        }
                                                }

                                                for(var i = loadedNum; i < nextSet; i++) {
                                                        $(container).append('' +
                                                                '<li class="thumb-holder" data-index="' + i + '">' +
                                                                        '<div class="thumb loading' + ((props.type == "videos") ? " video" : "") + '" tabindex="0" data-index="' + i + '">' +
                                                                                '<img src="' + data[i].thumb + '" alt="" onload="$(this).parent().removeClass(\'loading\');" />' +
                                                                        '</div>' +
                                                                '</li>' +
                                                        '');
                                                }

                                                if(props.type == "images") {
                                                        this.ImagesLoaded += this.ThumbsToLoad;
                                                } else {
                                                        this.VideosLoaded += this.ThumbsToLoad;
                                                }
                                        }
                                }
                        }
                }
                
        },
        
        "Events": function() {

                var _this = this;
                
                $(document).on("click keydown", ".thumb", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                if($("#media-overlay-outer").attr("aria-hidden") == "true") {
                                        $("#media-overlay-outer").attr("aria-hidden", "false");
                                        $("body").css("overflow", "hidden");
                                }
                                
                                var index = parseInt($(this).attr("data-index"));
                                
                                if($(this).hasClass("video")) {
                                        $("#media-overlay").attr("data-type", "video").html('' +
                                                '<video controls autoplay data-index="' + index + '">' +
                                                        '<source src="' + buildVideos[index].src + '" type="video/mp4">' +
                                                '</video>' +
                                        '');
                                } else {
                                        $("#media-overlay").attr("data-type", "image").html('<img src="' + buildImages[index].original + '" data-index="' + index + '" alt="" />');
                                }
                        }
                });
                
                $(document).on("click keydown", ".media-overlay-button", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                var mediaType = $("#media-overlay").attr("data-type");
                                var index = (mediaType == "image") ? parseInt($("#media-overlay img").attr("data-index")) : parseInt($("#media-overlay video").attr("data-index"));
                                var data = (mediaType == "image") ? buildImages : buildVideos;
                                
                                if($(this).hasClass("next")) {
                                        if(index == (data.length - 1)) {
                                                index = 0;
                                        } else {
                                                index++;
                                        }
                                } else {
                                        if(index == 0) {
                                                index = data.length - 1;
                                        } else {
                                                index--;
                                        }
                                }
                                
                                if(mediaType == "image") {
                                        $("#media-overlay").html('<img src="' + data[index].original + '" data-index="' + index + '" alt="" />');
                                } else {
                                        $("#media-overlay").html('' +
                                                '<video controls autoplay data-index="' + index + '">' +
                                                        '<source src="' + data[index].src + '" type="video/mp4">' +
                                                '</video>' +
                                        '');
                                }
                        }
                });
                
                $(document).on("keydown", function(e) {
                        if($("#media-overlay-outer").attr("aria-hidden") == "false") {
                                if(e.keyCode == _this.KeyCodes.left) {
                                        e.preventDefault();
                                        
                                        var mediaType = $("#media-overlay").attr("data-type");
                                        var index = (mediaType == "image") ? parseInt($("#media-overlay img").attr("data-index")) : parseInt($("#media-overlay video").attr("data-index"));
                                        var data = (mediaType == "image") ? buildImages : buildVideos;
                                        
                                        if(index == 0) {
                                                index = data.length - 1;
                                        } else {
                                                index--;
                                        }
                                        
                                        if(mediaType == "image") {
                                                $("#media-overlay").html('<img src="' + data[index].original + '" data-index="' + index + '" alt="" />');
                                        } else {
                                                $("#media-overlay").html('' +
                                                        '<video controls autoplay data-index="' + index + '">' +
                                                                '<source src="' + data[index].src + '" type="video/mp4">' +
                                                        '</video>' +
                                                '');
                                        }
                                }
                                
                                if(e.keyCode == _this.KeyCodes.right) {
                                        e.preventDefault();
                                        
                                        var mediaType = $("#media-overlay").attr("data-type");
                                        var index = (mediaType == "image") ? parseInt($("#media-overlay img").attr("data-index")) : parseInt($("#media-overlay video").attr("data-index"));
                                        var data = (mediaType == "image") ? buildImages : buildVideos;
                                        
                                        if(index == (data.length - 1)) {
                                                index = 0;
                                        } else {
                                                index++;
                                        }
                                        
                                        if(mediaType == "image") {
                                                $("#media-overlay").html('<img src="' + data[index].original + '" data-index="' + index + '" alt="" />');
                                        } else {
                                                $("#media-overlay").html('' +
                                                        '<video controls autoplay data-index="' + index + '">' +
                                                                '<source src="' + data[index].src + '" type="video/mp4">' +
                                                        '</video>' +
                                                '');
                                        }
                                }
                                
                                if(e.keyCode == _this.KeyCodes.esc) {
                                        e.preventDefault();
                                        
                                        $("#media-overlay-outer").attr("aria-hidden", "true");
                                        $("#media-overlay").attr("data-type", "");
                                        $("body").css("overflow", "");
                                        setTimeout(function() {
                                                $("#media-overlay").empty();
                                        }, 400);
                                }
                        }
                });
                
                $(document).on("click keydown", "#media-overlay-close", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                $("#media-overlay-outer").attr("aria-hidden", "true");
                                $("#media-overlay").attr("data-type", "");
                                $("body").css("overflow", "");
                                setTimeout(function() {
                                        $("#media-overlay").empty();
                                }, 400);
                        }
                });

                $(document).on("click keydown", "#back-to-top", function(e) {
                        if(_this.AllyClick(e)) {
                                e.preventDefault();
                                
                                $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                });
                
        },
        
        "AllyClick": function(event) {

                if(event.type == "click") {
                        return true;
                } else if(event.type == "keydown" && (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter)) {
                        return true;
                } else {
                        return false;
                }

        }

};
