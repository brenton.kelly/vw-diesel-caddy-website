// https://ediker.com/

/*
var imageNum = 685;
var images = '';

for(var i = 1; i <= imageNum; i++) {
	images += '\t{\n';
	images += '\t\t"original": "images/originals/IMG' + i + '.jpg",\n';
	images += '\t\t"thumb": "images/thumbs/IMG' + i + '.jpg",\n';
	images += '\t\t"caption": "",\n';
	images += '\t\t"tags": []\n';
	images += '\t}';
	
	if(i != imageNum) {
		images += ',';
	}
	images += '\n';
}
console.log(images);
*/

var buildImages = [
  {
		"original": "images/originals/IMG1.jpg",
		"thumb": "images/thumbs/IMG1.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG2.jpg",
		"thumb": "images/thumbs/IMG2.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG3.jpg",
		"thumb": "images/thumbs/IMG3.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG4.jpg",
		"thumb": "images/thumbs/IMG4.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG5.jpg",
		"thumb": "images/thumbs/IMG5.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG6.jpg",
		"thumb": "images/thumbs/IMG6.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG7.jpg",
		"thumb": "images/thumbs/IMG7.jpg",
		"caption": "",
		"tags": []
	},
	{
		"original": "images/originals/IMG8.jpg",
		"thumb": "images/thumbs/IMG8.jpg",
		"caption": "",
		"tags": []
	}
];